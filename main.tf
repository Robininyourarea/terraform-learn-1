# provider
provider "aws" {
  region = "eu-west-3"
  access_key = "SDJKAOFKASJFSDHASCSK05"
  secret_key = "disdoDodjasj3480g=0+0ksdfkpjJIasff]PSFO"
}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}

# vpc
resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

# subnet
resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-subnet"
  }
}

# internet gateway (virtual modem of vpc)
resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_apc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }

}

# route table (virtual router of vpc)
resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_apc.myapp-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

